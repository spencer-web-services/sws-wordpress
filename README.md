# sws-wordpress

Docker compose file for setting up a testing WordPress install environment.

Create a self-signed SSL certificate:
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl-cert-snakeoil.key \
-out ssl-cert-snakeoil.pem \
-subj "/C=US/ST=AZ/L=Gilbert/O=Spencer Web Services/OU=IT Department/CN=localhost"
```